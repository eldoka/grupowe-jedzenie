ActiveAdmin.register User do
  index do
    selectable_column
    column :email
    column :admin
    column :created_at
    actions
  end


  form do |f|
    f.inputs do
      f.input :email
      f.input :admin
    end
    f.actions
  end

  controller do
    def permitted_params
      params.permit!
    end

  end
end
