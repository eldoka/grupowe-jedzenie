class MembershipsController < ApplicationController
  before_action :set_membership, only: [:show, :set_admin, :remove_admin, :destroy]
  before_action :set_group
  before_action :authenticate_user!
  before_action :authorize_user, only: [:index]
  before_action :authorize_admin, except: [:index]

  rescue_from Pundit::NotAuthorizedError do
    flash[:error] = "Access denied"
    redirect_to group_memberships_path(@group)
  end

  def index
    @memberships = @group.memberships
  end

  def show
    unless @group.memberships.include? @membership
      flash[:error] = "Requested membership not found"
      redirect_to group_memberships_path(@group)
    end
  end

  def new
    @users = @group.available_users
  end


  def create
    if params[:user_id]
      @group.user_ids += params[:user_ids]

      respond_to do |format|
        format.html { redirect_to group_memberships_path, notice: 'Memberships was successfully created.' }
      end
    else
      redirect_to group_memberships_path(@group)
    end
  end

  def set_admin
    @group.has_membership(current_user).set_admin(@membership)
    redirect_to group_memberships_path(@group)
  end

  def remove_admin
    authorize @membership, :remove_admin?
    @membership.update(admin: false)
    redirect_to group_memberships_path(@group)
  end

  def destroy
    @membership.destroy
    respond_to do |format|
      format.html { redirect_to group_memberships_url(@group), notice: 'Membership was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def authorize_admin
      authorize @group, :admin?
    end

    def authorize_user
      authorize @group, :user?
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_membership
      @membership = Membership.find(params[:id])
    end

    def set_group
      @group = Group.find(params[:group_id])
    end
end
