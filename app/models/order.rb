class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
  belongs_to :restaurant
  has_many :order_items, dependent: :destroy

  validates :user_id, :group_id, :restaurant_id, presence: true
end
