class Membership < ActiveRecord::Base
  belongs_to :user
  belongs_to :group

  validates :user_id, uniqueness: { scope: :group_id }
  validates :user, :group, presence: true

  def set_admin(user)
    user.update(admin: true) if self.admin || self.user.admin
  end
end
