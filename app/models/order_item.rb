class OrderItem < ActiveRecord::Base
  belongs_to :user
  belongs_to :order

  validates :user_id, :order_id, :name, :quantity, :price, presence: true
  validates :name, length: { maximum: 255 }
  validates :quantity, :price, numericality: true
end
