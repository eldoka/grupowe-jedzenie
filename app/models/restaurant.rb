class Restaurant < ActiveRecord::Base
  has_many :orders

  validates :name, presence: true, length: { maximum: 255 }, uniqueness: true
  validates :description, length: { maximum: 500 }
  validate :check_url

  private

  def check_url
    errors.add(:menu_url, 'must be valid url address') unless menu_url =~ /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/ || menu_url.blank?
  end
end
