class Group < ActiveRecord::Base
  has_many :memberships, dependent: :destroy
  has_many :users, through: :memberships
  has_many :orders, dependent: :destroy

  validates :name, presence: true, length: { maximum: 255 }, uniqueness: true
  validates :description, length: { maximum: 500 }

  def admin?(user)
    membership = self.has_membership(user)
    membership.admin if membership
  end

  def set_admin(user) # TODO przerobic jakos ladniej
    membership = self.has_membership(user)
    membership.update(admin: true) if membership
  end

  def has_membership(user)
    self.memberships.find_by(user_id: user.id)
  end

  def available_users
    ids = Membership.where(group_id: id).pluck(:user_id)
    User.where('id not in (?)', ids)
  end

end
