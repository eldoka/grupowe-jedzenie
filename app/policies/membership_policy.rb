MembershipPolicy = Struct.new(:user, :membership) do
  def remove_admin?
    user.admin || user.id == membership.user_id
  end

end