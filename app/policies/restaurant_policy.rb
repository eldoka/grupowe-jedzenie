RestaurantPolicy = Struct.new(:user, :restaurant) do
  def admin?
    user.admin
  end
end