GroupPolicy = Struct.new(:user, :group) do
  def admin?
    group.admin?(user) || user.admin
  end

  def user?
    group.has_membership(user) || admin?
  end
end