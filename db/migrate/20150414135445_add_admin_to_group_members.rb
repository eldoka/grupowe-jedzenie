class AddAdminToGroupMembers < ActiveRecord::Migration
  def change
    rename_table :groups_users, :group_members
    add_column :group_members, :admin, :boolean, default: false
    add_column :group_members, :id, :primary_key
  end
end
