class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :user_id
      t.integer :order_id
      t.string :name
      t.integer :quantity
      t.float :price

      t.timestamps null: false
    end

    add_index :order_items, :user_id
    add_index :order_items, :order_id
  end
end
