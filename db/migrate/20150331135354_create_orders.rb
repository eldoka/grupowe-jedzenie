class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :group_id
      t.integer :user_id
      t.integer :restaurant_id

      t.timestamps null: false
    end

    add_index :orders, :group_id
    add_index :orders, :user_id
    add_index :orders, :restaurant_id
  end
end
