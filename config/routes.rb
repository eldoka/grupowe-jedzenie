Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'welcome#index'

  resources :order_items

  resources :groups do
    resources :memberships, expect: [:edit, :update]

    resources :orders
  end

  post "/groups/:group_id/memberships/:id/set_admin",       to: "memberships#set_admin",        as: "group_membership_set_admin"
  post "/groups/:group_id/memberships/:id/remove_admin",    to: "memberships#remove_admin",     as: "group_membership_remove_admin"

  resources :restaurants

  devise_for :users
end
